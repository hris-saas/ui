<?php

namespace HRis\UI\Exceptions;

use Spatie\Ignition\Contracts\Solution;
use HRis\UI\Solutions\UIAssetsNotFoundSolution;
use Spatie\Ignition\Contracts\ProvidesSolution;
use Illuminate\Contracts\Filesystem\FileNotFoundException;

class UIAssetsNotFoundException extends FileNotFoundException implements ProvidesSolution
{
    public function getSolution(): Solution
    {
        return new UIAssetsNotFoundSolution();
    }
}
